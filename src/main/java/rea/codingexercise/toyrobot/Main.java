package rea.codingexercise.toyrobot;

import rea.codingexercise.toyrobot.model.impl.Robot;
import rea.codingexercise.toyrobot.model.environment.TableTop;
import rea.codingexercise.toyrobot.utils.RobotProtocolUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Reads a file and feeds correct instructions to a toy robot.
 * See README.md for full description.
 * <p>
 * Created by Bertrand on 26/09/2014.
 */
public class Main {

    private static final LogManager logManager = LogManager.getLogManager();
    private static final Logger LOGGER = Logger.getLogger("confLogger");

    static {
        try {
            logManager.readConfiguration(new FileInputStream("./src/main/resources/config/logging.properties"));
        } catch (IOException exception) {
            LOGGER.log(Level.SEVERE, "Error in loading configuration", exception);
        }
    }

    //default test file
    private static final String DEFAULT_FILE = "./src/main/resources/testRobot.txt";
    private static final TableTop DEFAULT_TABLETOP = new TableTop(5, 5);

    public static void main(String[] args) {
        showUsage();
        Path testFilePath;
        //we were passed a file, hopefully
        if (args.length >= 1) {
            testFilePath = Paths.get(args[0]);
            if (!Files.isReadable(testFilePath)) {
                testFilePath = useDefaultFile();
            } else {
                output("Using file: " + testFilePath);
            }
        } else {
            testFilePath = useDefaultFile();
        }
        try {
            try (Stream<String> lines = Files.lines(testFilePath)) {
                Robot rob = new Robot();
                rob.setTableTop(DEFAULT_TABLETOP);
                lines.forEach(line -> {
                    String[] commands = line.split(" ");
                    RobotProtocolUtils.translateToRobot(commands, rob).ifPresent(Main::output);
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void showUsage() {
        output("=================================================================");
        output("Toy robot");
        output("Place and move a toy robot on a 5x5 table top");
        output("Pass file as argument, default file is in resources/testRobot.txt");
        output("=================================================================");
        output("");
    }

    private static Path useDefaultFile() {
        output("No argument or invalid argument provided, using default file " + DEFAULT_FILE);
        return Paths.get(DEFAULT_FILE);
    }

    private static void output(String message) {
        if (message != null) {
            System.out.println(message);
        }
    }
}
