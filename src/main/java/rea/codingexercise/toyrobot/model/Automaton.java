package rea.codingexercise.toyrobot.model;

import rea.codingexercise.toyrobot.exception.RobotException;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Position;
import rea.codingexercise.toyrobot.model.environment.TableTop;
import rea.codingexercise.toyrobot.model.environment.Turn;

public interface Automaton {

    public void move() throws RobotException;

    public void turn(Turn turn) throws RobotException;

    public void placeOnTableTop(Position position, Orientation orientation) throws RobotException;

    public void setTableTop(TableTop table);

    public String report() throws RobotException;

    public Orientation getFacing() throws RobotException;

    public Position getPosition() throws RobotException;
}