package rea.codingexercise.toyrobot.model.environment;

/**
 * Table to put the robot on.
 * (0,0) : bottom left corner
 * (width, height) : top right corner
 *
 * Created by Bertrand on 26/09/2014.
 */
public class TableTop {

    /** The width of the table. */
    private int width;

    /** The height of the table. */
    private int height;

    /**
     * Instantiates a new table.
     *
     * @param x width of the table
     * @param y height of the table
     */
    public TableTop(int x, int y){
        super();
        this.setWidth(x);
        this.setHeight(y);
    }

    /**
     * Gets the height.
     *
     * @return returns the height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height.
     *
     * @param height height to set.
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Gets the width.
     *
     * @return returns the width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width.
     *
     * @param width width to set.
     */
    public void setWidth(int width) {
        this.width = width;
    }

}
