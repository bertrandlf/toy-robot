package rea.codingexercise.toyrobot.model.environment;

/**
 * Position representation, on a plane.
 *
 * Created by Bertrand on 26/09/2014.
 */
public class Position {

    // horizontal position
    private int x;
    // vertical position
    private int y;

    /**
     * Constructor with horizontal and vertical components.
     *
     * @param x horizontal
     * @param y vertical
     */
    public Position(int x, int y) {
        super();

        this.x = x;
        this.y = y;
    }

    /**
     * @return Returns the horizontal part of the position.
     */
    public int getX() {
        return x;
    }

    /**
     * @return y, the vertical part of the position.
     */
    public int getY() {
        return y;
    }

    /**
     * Return the string representation of a position.
     *
     * @return position String
     */
    public String toString() {
        return getX() + "," + getY();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return x == position.x && y == position.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
