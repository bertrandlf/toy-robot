package rea.codingexercise.toyrobot.model.environment;

/**
 * Turn right or left
 *
 * Created by Bertrand on 27/09/2014.
 */
public enum Turn {

    LEFT("LEFT"),
    RIGHT("RIGHT");

    private String value;

    Turn(String turn) {
        setValue(turn);
    }

    /**
     * @return value the turn value
     * */
    public String getValue() {
        return value;
    }

    /**
     * @param value set turn value
     * */
    public void setValue(String value) {
        this.value = value;
    }

}
