package rea.codingexercise.toyrobot.exception;

/**
 * Any exception the robot encounters will be wrapped in this one.
 *
 * Created by Bertrand on 26/09/2014.
 */
public class RobotException extends Exception {

    /**
     * Instantiates a new robot exception.
     *
     * @param message what happened
     * @param throwable encapsulated exception
     */
    public RobotException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * Instantiates a new robot exception.
     *
     * @param message what happened
     */
    public RobotException(String message) {
        super(message);
    }

}
