package rea.codingexercise.toyrobot.utils;

import org.junit.Test;
import rea.codingexercise.toyrobot.model.RobotTest;
import rea.codingexercise.toyrobot.model.environment.Position;
import static junit.framework.Assert.assertEquals;
/**
 * Test robot utils
 *
 * Created by Bertrand on 27/09/2014.
 */
public class RobotActionsUtilsTest extends RobotTest {

    @Test
    public void shouldMoveOf() throws Exception {
        //move from south west corner to north east corner
        assertEquals(
                RobotActionsUtils.moveOf(SOUTH_WEST_CORNER, new Position(TABLE_WIDTH, TABLE_HEIGHT)),
                NORTH_EAST_CORNER);
        //move from north west corner to south east corner
        assertEquals(
                RobotActionsUtils.moveOf(NORTH_WEST_CORNER, new Position(TABLE_WIDTH, -TABLE_HEIGHT)),
                SOUTH_EAST_CORNER);
        //move from south east corner to south west corner
        assertEquals(
                RobotActionsUtils.moveOf(SOUTH_EAST_CORNER, new Position(-TABLE_WIDTH, 0)),
                SOUTH_WEST_CORNER);
    }

}