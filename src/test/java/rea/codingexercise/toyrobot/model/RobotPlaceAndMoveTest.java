package rea.codingexercise.toyrobot.model;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import rea.codingexercise.toyrobot.exception.RobotException;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Position;

import static org.junit.Assert.assertTrue;

/**
 * Robot tests, for placing the robot and moving it.
 * <p>
 * Created by Bertrand on 26/09/2014.
 */
@RunWith(Theories.class)
public class RobotPlaceAndMoveTest extends RobotTest {

    @DataPoints
    public static Position[] tableCorners() {
        return new Position[]{SOUTH_EAST_CORNER, SOUTH_WEST_CORNER,
                NORTH_EAST_CORNER, NORTH_WEST_CORNER};
    }

    @Test(expected = RobotException.class)
    public void shouldNotBePlaceableWithoutTable() throws Exception {
        rob.setTableTop(null);
        rob.placeOnTableTop(SOUTH_WEST_CORNER, Orientation.NORTH);
    }

    @Test(expected = RobotException.class)
    public void shouldNotBePlaceableOutOfTableWidthEast() throws Exception {
        Position notOnTable = new Position(TABLE_WIDTH + 1, TABLE_HEIGHT);
        rob.placeOnTableTop(notOnTable, Orientation.EAST);
    }

    @Test(expected = RobotException.class)
    public void shouldNotBePlaceableOutOfTableWidthWest() throws Exception {
        Position notOnTable = new Position(-1, TABLE_HEIGHT);
        rob.placeOnTableTop(notOnTable, Orientation.EAST);
    }

    @Test(expected = RobotException.class)
    public void shouldNotBePlaceableOutOfTableHeightNorth() throws Exception {
        Position notOnTable = new Position(TABLE_WIDTH, TABLE_HEIGHT + 1);
        rob.placeOnTableTop(notOnTable, Orientation.SOUTH);
    }

    @Test(expected = RobotException.class)
    public void shouldNotBePlaceableOutOfTableHeightSouth() throws Exception {
        Position notOnTable = new Position(TABLE_WIDTH, -1);
        rob.placeOnTableTop(notOnTable, Orientation.SOUTH);
    }

    @Theory
    public void shouldBePlaceableOnTableTopCorners(Position corner) throws Exception {
        rob.placeOnTableTop(corner, Orientation.NORTH);
    }

    @Test
    public void shouldMoveForwardNorth() throws Exception {
        testMove(new MoveExpectation().position(SOUTH_WEST_CORNER)
                .orientation(Orientation.NORTH)
                .expectedPosition(new Position(0, 1)));
    }

    @Test
    public void shouldMoveForwardEast() throws Exception {
        testMove(new MoveExpectation().position(SOUTH_WEST_CORNER)
                .orientation(Orientation.EAST)
                .expectedPosition(new Position(1, 0)));
    }

    @Test
    public void shouldMoveForwardSouth() throws Exception {
        testMove(new MoveExpectation().position(new Position(0, 1))
                .orientation(Orientation.SOUTH)
                .expectedPosition(SOUTH_WEST_CORNER));
    }

    @Test
    public void shouldMoveForwardWest() throws Exception {
        testMove(new MoveExpectation().position(new Position(1, 0))
                .orientation(Orientation.WEST)
                .expectedPosition(SOUTH_WEST_CORNER));
    }

    @Test
    public void shouldNotFallNorth() throws Exception {
        testMove(new MoveExpectation().position(NORTH_EAST_CORNER)
                .orientation(Orientation.NORTH)
                .expectedPosition(NORTH_EAST_CORNER));
    }

    @Test
    public void shouldNotFallEast() throws Exception {
        testMove(new MoveExpectation().position(NORTH_EAST_CORNER)
                .orientation(Orientation.EAST)
                .expectedPosition(NORTH_EAST_CORNER));
    }

    @Test
    public void shouldNotFallSouth() throws Exception {
        testMove(new MoveExpectation().position(SOUTH_WEST_CORNER)
                .orientation(Orientation.SOUTH)
                .expectedPosition(SOUTH_WEST_CORNER));
    }

    @Test
    public void shouldNotFallWest() throws Exception {
        testMove(new MoveExpectation().position(SOUTH_WEST_CORNER)
                .orientation(Orientation.WEST)
                .expectedPosition(SOUTH_WEST_CORNER));
    }

    private void testMove(MoveExpectation moveExpectation) throws Exception {
        rob.placeOnTableTop(moveExpectation.getPosition(), moveExpectation.getOrientation());
        rob.move();
        Position nowAt = rob.getPosition();
        assertTrue("Expected to be at " + moveExpectation.getExpectedPosition() + " but at " + nowAt + " instead",
                moveExpectation.getExpectedPosition().equals(nowAt));
    }

    private class MoveExpectation {

        private Position position;
        private Orientation orientation;
        private Position expectedPosition;

        public MoveExpectation(){
            super();
        }

        public MoveExpectation position(Position pos) {
            this.position = pos;
            return this;
        }

        public MoveExpectation orientation(Orientation orientation) {
            this.orientation = orientation;
            return this;
        }

        public MoveExpectation expectedPosition(Position expected) {
            this.expectedPosition = expected;
            return this;
        }

        public Position getPosition() {
            return position;
        }

        public Orientation getOrientation() {
            return orientation;
        }

        public Position getExpectedPosition() {
            return expectedPosition;
        }

    }

}