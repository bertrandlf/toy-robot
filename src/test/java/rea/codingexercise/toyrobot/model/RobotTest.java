package rea.codingexercise.toyrobot.model;

import org.junit.Before;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Position;
import rea.codingexercise.toyrobot.model.environment.TableTop;
import rea.codingexercise.toyrobot.model.environment.Turn;
import rea.codingexercise.toyrobot.model.impl.Robot;

import static org.junit.Assert.assertTrue;

/**
 * Variables and methods common to robot test classes.
 *
 * Created by Bertrand on 27/09/2014.
 */
public class RobotTest {

    public static final int TABLE_HEIGHT = 5;
    public static final int TABLE_WIDTH = 5;

    public static final Position SOUTH_WEST_CORNER = new Position(0, 0);
    public static final Position NORTH_WEST_CORNER = new Position(0, TABLE_HEIGHT);
    public static final Position NORTH_EAST_CORNER = new Position(TABLE_WIDTH, TABLE_HEIGHT);
    public static final Position SOUTH_EAST_CORNER = new Position(TABLE_WIDTH, 0);

    protected Automaton rob;
    protected TableTop table;

    @Before
    public void setUp() {
        rob = new Robot();
        table = new TableTop(TABLE_WIDTH, TABLE_HEIGHT);
        rob.setTableTop(table);
    }

    protected void testTurn(Orientation facingBeforeTurn, Orientation expectedAfterTurn, Turn turn) throws Exception {
        //it does not matter where we put the robot on the table
        rob.placeOnTableTop(SOUTH_WEST_CORNER, facingBeforeTurn);
        rob.turn(turn);
        Orientation nowFacing = rob.getFacing();
        assertTrue("Expected " + expectedAfterTurn + " but got " + nowFacing, nowFacing.equals(expectedAfterTurn));
    }
}
