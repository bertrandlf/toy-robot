package rea.codingexercise.toyrobot.model;

import org.junit.Test;
import rea.codingexercise.toyrobot.model.environment.Orientation;

import static org.junit.Assert.assertEquals;

/**
 * Test the report toy robot gives.
 * <p>
 * Created by Bertrand on 27/09/2014.
 */
public class RobotReportTest extends RobotTest {

    @Test
    public void shouldReport() throws Exception {
        rob.placeOnTableTop(SOUTH_EAST_CORNER, Orientation.EAST);
        String robReport = rob.report();
        String expectedReport = TABLE_WIDTH + ",0 EAST";
        assertEquals(robReport, expectedReport);
    }

}
