package rea.codingexercise.toyrobot.model;

import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Turn;

import java.util.HashMap;
import java.util.Map;

/**
 * Robot tests for turning left.
 * <p>
 * Created by Bertrand on 27/09/2014.
 */
@RunWith(Theories.class)
public class RobotTurnLeftTest extends RobotTest {

    private Map<Orientation, Orientation> left;

    @DataPoints
    public static Orientation[] orientation =
            {Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST};

    @Before
    @Override
    public void setUp() {
        super.setUp();
        left = new HashMap<>();
        //initialize expected behaviour
        left.put(Orientation.NORTH, Orientation.WEST);
        left.put(Orientation.EAST, Orientation.NORTH);
        left.put(Orientation.SOUTH, Orientation.EAST);
        left.put(Orientation.WEST, Orientation.SOUTH);
    }

    @Theory
    public void shouldTurnLeft(Orientation orientation) throws Exception {
        testTurn(orientation, left.get(orientation), Turn.LEFT);
    }

}
