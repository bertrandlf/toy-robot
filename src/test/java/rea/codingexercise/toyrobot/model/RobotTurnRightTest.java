package rea.codingexercise.toyrobot.model;

import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Turn;

import java.util.HashMap;
import java.util.Map;

/**
 * Robot tests for turning right.
 * The reason we separate turning right and left is to leverage JUnit theories.
 * <p>
 * We can only define one set of DataPoints per type.
 * We could use private classes to have it all in one class, but clarity would suffer.
 * <p>
 * Created by Bertrand on 27/09/2014.
 */
@RunWith(Theories.class)
public class RobotTurnRightTest extends RobotTest {

    private Map<Orientation, Orientation> right;

    @DataPoints
    public static Orientation[] orientation =
            {Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST};

    @Before
    @Override
    public void setUp() {
        super.setUp();
        right = new HashMap<>();
        //initialize expected behaviour
        right.put(Orientation.NORTH, Orientation.EAST);
        right.put(Orientation.EAST, Orientation.SOUTH);
        right.put(Orientation.SOUTH, Orientation.WEST);
        right.put(Orientation.WEST, Orientation.NORTH);
    }

    @Theory
    public void shouldTurnRight(Orientation orientation) throws Exception {
        testTurn(orientation, right.get(orientation), Turn.RIGHT);
    }

}
